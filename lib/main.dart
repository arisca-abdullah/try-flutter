import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My First Flutter App',
          style: TextStyle(
              fontFamily: "Montserrat",
              fontWeight: FontWeight.w700
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.red[400],
      ),
      body: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Image.asset('assets/beach.jpg'),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Text(
                  '1',
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              color: Colors.blue,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Text(
                '2',
                style: TextStyle(
                    color: Colors.white
                ),
              ),
              color: Colors.pink,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Text(
                '3',
                style: TextStyle(
                    color: Colors.white
                ),
              ),
              color: Colors.purple,
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(
          Icons.add
        ),
        backgroundColor: Colors.red[400],
      ),
    );
  }
}
